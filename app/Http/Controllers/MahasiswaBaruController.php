<?php

namespace App\Http\Controllers;

use App\Mahasiswa;
use Illuminate\Http\Request;

class MahasiswaBaruController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mahasiswa = Mahasiswa::latest()->paginate(5);

        return view('Mahasiswa.index')->with('mahasiswa',$mahasiswa);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Mahasiswa.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $create = Mahasiswa::create($request->all());
        return redirect ('mahasiswa');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Mahasiswa  $mahasiswa
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mhsw = Mahasiswa::find($id);
        
        return view ('Mahasiswa.show')->with('mhsw',$mhsw);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Mahasiswa  $mahasiswa
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $m= Mahasiswa::find($id);
         return view('Mahasiswa.edit')->with('m',$m);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mahasiswa  $mahasiswa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = Mahasiswa::where('id',$request->id)->update([
            'nisn' => $request->nisn,
            'nama_depan' => $request->nama_depan,
            'nama_belakang' => $request->nama_belakang,
            'jk' => $request->jk,
            'alamat' => $request->alamat,
            'provinsi' => $request->provinsi,
            'kabupaten' => $request->kabupaten,
            'asal_sekolah' => $request->asal_sekolah,
            'nilai_rata' => $request->nilai_rata,
        ]);
        return redirect('mahasiswa');
        // $request->validate
  
        // $mahasiswa->update($request->all());
  
        // return redirect('mahasiswa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Mahasiswa  $mahasiswa
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $del=Mahasiswa::where('id' ,$id)->delete();
  
        return redirect('mahasiswa')->with('success','Mahasiswa deleted successfully');
    }
}
