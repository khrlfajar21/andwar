<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('products','ProductController');
Route::get('mahasiswa','MahasiswaBaruController@index');
Route::get('create/mhsw','MahasiswaBaruController@create');
Route::post('/store','MahasiswaBaruController@store');
Route::get('mahasiswa/edit/{id}','MahasiswaBaruController@edit');
Route::post('/update/mhsw','MahasiswaBaruController@update');
Route::get('show/{id}','MahasiswaBaruController@show');
Route::get('mahasiswa/destroy/{id}','MahasiswaBaruController@destroy');


Route::get('items','ItemsController@index');
Route::get('items/buat','ItemsController@buat');
Route::post('items/store','ItemsController@store');
Route::get('destroy/{id}','ItemsController@destroy');
Route::get('mengedit/data/{id}','ItemsController@mengedit');
Route::post('update/items','ItemsController@update');
Route::get('items/show/{id}','ItemsController@show');

Route::get('barang','BarangController@index');
Route::get('barang/create','BarangController@create');
Route::post('barang/store','BarangController@store');
Route::get('barang/edit/{id}','BarangController@edit');
Route::post('barang/update','BarangController@update');
Route::get('barang/destroy/{id}','BarangController@destroy');
Route::get('barang/show/{id}','BarangController@show');

